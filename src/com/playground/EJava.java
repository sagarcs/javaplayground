package com.playground;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

public class EJava {
  public static void main(String[] args) {
    Integer no = null ;
    //Integer anotherNo = Objects.requireNonNull(no, "number must not be null");
    String sag = "sagar" ;
    Optional<String> a = Optional.ofNullable(sag);
    a.ifPresent(new Consumer<String>() {
      @Override
      public void accept(final String s) {
        System.out.println(s);
      }
    });
  }
}
