package com.playground.designpattern.iterator;

import java.util.Iterator;

public interface SongIterator {
  public Iterator getIterator();
}
