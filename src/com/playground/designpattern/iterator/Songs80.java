package com.playground.designpattern.iterator;

import com.playground.designpattern.composite.Song;
import java.util.Arrays;
import java.util.Iterator;

public class Songs80 implements SongIterator{
  Song[] songs ;
  public Songs80(Song[] songs) {
    this.songs = songs;
  }

  @Override
  public Iterator getIterator() {
    return Arrays.asList(songs).iterator();
  }
}
