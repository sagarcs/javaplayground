package com.playground.designpattern.iterator;

import com.playground.designpattern.composite.Song;
import java.util.ArrayList;

public class Driver {
  public static void main(String[] args) {
    Song song1 = new Song("starboy", "weeknd", 1970);
    Song song2 = new Song("BadBlood", "Taylor Swift", 170);

    ArrayList songsArrayList = new ArrayList();
    songsArrayList.add(song1);
    songsArrayList.add(song2);
    SongIterator songIteratorFr70 = new Songs70(songsArrayList);

    Song song3 = new Song("starboy80", "weeknd", 1980);
    Song song4 = new Song("BadBlood80", "Taylor Swift", 1980);
    Song[] songsArray = new Song[]{song3, song4};
    SongIterator songIteratorFr80 = new Songs80(songsArray);

    Discjockey discjockey = new Discjockey(songIteratorFr70, songIteratorFr80);
    discjockey.playSongs();


  }
}
