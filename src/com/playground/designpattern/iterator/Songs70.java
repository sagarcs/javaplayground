package com.playground.designpattern.iterator;

import com.playground.designpattern.composite.Song;
import java.util.ArrayList;
import java.util.Iterator;

public class Songs70 implements SongIterator {
  ArrayList<Song> songsof70;

  public Songs70(ArrayList songsof70) {
    this.songsof70 = songsof70;
  }

  @Override
  public Iterator getIterator() {
    return songsof70.iterator();
  }
}
