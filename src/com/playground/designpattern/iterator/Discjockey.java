package com.playground.designpattern.iterator;

import com.playground.designpattern.composite.Song;
import java.util.Iterator;

public class Discjockey {

  SongIterator songsof70;
  SongIterator songsof80;

  public Discjockey(SongIterator songslist1, SongIterator songslist2) {
    this.songsof70 = songslist1;
    this.songsof80 = songslist2;
  }

  public void playSongs() {
    Iterator songsof70sitr = songsof70.getIterator();
    Iterator songsof80sitr = songsof80.getIterator();

    System.out.println("printing songs of 70:");
    while(songsof70sitr.hasNext()) {
      Song song = (Song) songsof70sitr.next();
      song.displaySongInfo();
    }

    System.out.println("printing songs of 80:");
    while(songsof80sitr.hasNext()) {
      Song song = (Song) songsof80sitr.next();
      song.displaySongInfo();
    }
  }

}
