package com.playground.designpattern.statemachine;

import com.playground.designpattern.proxypattern.ProtectedATMMachine;

public class ATMMachine implements ATMState,
                                   //proxy design pattern
                                   ProtectedATMMachine
{

  private ATMState hasCard;
  private ATMState noCard;
  private ATMState hasPin;
  private ATMState noMoney;
  private ATMState currentState;

  public int availableCashInATM = 0;
  public boolean isCorrectPinEntered = false;

  @Override
  public String getATMState() {
    return getState();
  }

  @Override
  public String getState() {
    return currentState.getState();
  }

  @Override
  public int getATMBalance() {
    return availableCashInATM;
  }

  public ATMMachine() {
    hasCard = new HasCard(this);
    noCard = new NoCard(this);
    hasPin = new HasPin(this);
    noMoney = new NoCash(this);

    currentState = noCard;

    if(availableCashInATM <= 0) {
      currentState = noMoney;
    }

  }

  public void setATMState(final ATMState atmState) {
    this.currentState = atmState;
  }

  @Override
  public void insertCard() {
    currentState.insertCard();
  }

  @Override
  public void ejectCard() {
    currentState.ejectCard();
  }

  @Override
  public void insertPin(final int enteredPin) {
    currentState.insertPin(enteredPin);
  }

  @Override
  public void requestCash(final int requestedCash) {
    currentState.requestCash(requestedCash);
  }

  @Override
  public void loadATMWithCash(final int cashToLoad) {
    currentState.loadATMWithCash(cashToLoad);
  }

  public ATMState getHasCard() {
    return hasCard;
  }

  public ATMState getNoCard() {
    return noCard;
  }

  public ATMState getHasPin() {
    return hasPin;
  }

  public ATMState getNoMoney() {
    return noMoney;
  }
}
