package com.playground.designpattern.statemachine;

public class HasPin implements ATMState {

  @Override
  public String getState() {
    return "has pin state";
  }

  private ATMMachine atmMachine;

  public HasPin(final ATMMachine atmMachine) {
    this.atmMachine = atmMachine;
  }

  @Override
  public void insertCard() {
    System.out.println("card is already inserted");
  }

  @Override
  public void ejectCard() {
    System.out.println("ejecting your card");
    atmMachine.isCorrectPinEntered = false;
    atmMachine.setATMState(atmMachine.getNoCard());
  }

  @Override
  public void insertPin(final int enteredPin) {
    System.out.println("Pin has already been entered");
  }

  @Override
  public void requestCash(final int requestedCash) {
    if(atmMachine.availableCashInATM >= requestedCash) {
      System.out.println("collect your cash from the atm drawer");
      atmMachine.isCorrectPinEntered = false;
      atmMachine.availableCashInATM = atmMachine.availableCashInATM - requestedCash;
      if(atmMachine.availableCashInATM <= 0) {
        atmMachine.setATMState(atmMachine.getNoMoney());
      }
      atmMachine.ejectCard();
    } else {
      System.out.println("Not enough money");
      atmMachine.isCorrectPinEntered = false;
      atmMachine.ejectCard();
      atmMachine.setATMState(atmMachine.getNoMoney());
    }
  }

  @Override
  public void loadATMWithCash(final int cashToLoad) {
    System.out.println("can't load, eject card and try");
  }
}
