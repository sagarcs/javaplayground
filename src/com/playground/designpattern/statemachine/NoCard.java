package com.playground.designpattern.statemachine;

public class NoCard implements ATMState {
  private ATMMachine atmMachine;

  @Override
  public String getState() {
    return "no card state";
  }

  public NoCard(final ATMMachine atmMachine) {
    this.atmMachine= atmMachine;
  }

  @Override
  public void insertCard() {
    System.out.println("inserting card");
    this.atmMachine.setATMState(atmMachine.getHasCard());
    }

  @Override
  public void ejectCard() {
    System.out.println("enter card first");
  }

  @Override
  public void insertPin(final int enteredPin) {
    System.out.println("enter card first");
  }

  @Override
  public void requestCash(final int requestedCash) {
    System.out.println("enter card first");
  }

  @Override
  public void loadATMWithCash(final int cashToLoad) {
    this.atmMachine.availableCashInATM += cashToLoad;
  }
}
