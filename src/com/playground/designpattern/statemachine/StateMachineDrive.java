package com.playground.designpattern.statemachine;

public class StateMachineDrive {

  public static void main(String[] args) {
    ATMMachine atmMachine = new ATMMachine();
    atmMachine.insertCard();
    atmMachine.loadATMWithCash(10000);
    atmMachine.insertCard();
    atmMachine.insertPin(123);
    atmMachine.requestCash(1000);
    atmMachine.insertCard();
    atmMachine.insertPin(123);
    atmMachine.requestCash(909090);
  }



}


