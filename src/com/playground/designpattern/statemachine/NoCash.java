package com.playground.designpattern.statemachine;

public class NoCash implements ATMState {

  @Override
  public String getState() {
    return "no cash state";
  }

  private ATMMachine atmMachine;

  public NoCash(final ATMMachine atmMachine) {
    this.atmMachine = atmMachine;
  }

  @Override
  public void insertCard() {
    System.out.println("atm out of service");
  }

  @Override
  public void ejectCard() {
    System.out.println("atm out of service");
  }

  @Override
  public void insertPin(final int enteredPin) {
    System.out.println("atm out of service");
  }

  @Override
  public void requestCash(final int requestedCash) {
    System.out.println("atm out of service");
  }

  @Override
  public void loadATMWithCash(final int cashToLoad) {
    atmMachine.availableCashInATM += cashToLoad;
    atmMachine.setATMState(atmMachine.getNoCard());
  }
}
