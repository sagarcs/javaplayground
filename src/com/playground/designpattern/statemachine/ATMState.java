package com.playground.designpattern.statemachine;

public interface ATMState {
  public void insertCard();
  public void ejectCard();
  public void insertPin(int enteredPin);
  public void requestCash(int requestedCash);
  public void loadATMWithCash(int cashToLoad);
  public String getState();
}

