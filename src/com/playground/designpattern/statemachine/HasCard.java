package com.playground.designpattern.statemachine;

import javax.print.DocFlavor;

public class HasCard implements ATMState {
  @Override
  public String getState() {
    return "has card state";
  }

  private ATMMachine atmMachine;

  public HasCard(ATMMachine atmMachine) {
    this.atmMachine = atmMachine;
  }

  @Override
  public void insertCard() {
    System.out.println("card has already been inserted");
  }

  @Override
  public void ejectCard() {
    System.out.println("card has been ejected");
    atmMachine.setATMState(atmMachine.getHasCard());
  }

  @Override
  public void insertPin(int enteredPin) {
    if(enteredPin == 123) {
      System.out.println("right pin entered");
      atmMachine.isCorrectPinEntered = true;
      atmMachine.setATMState(atmMachine.getHasPin());
    } else {
      System.out.println("wrong pin entered, ejecting card");
      atmMachine.isCorrectPinEntered = false;
      atmMachine.setATMState(atmMachine.getNoCard());
    }
  }

  @Override
  public void requestCash(int requestedCash) {
    System.out.println("you need to enter pin first");
  }

  @Override
  public void loadATMWithCash(final int cashToLoad) {
    System.out.println("can't load, eject card first and try");
  }
}
