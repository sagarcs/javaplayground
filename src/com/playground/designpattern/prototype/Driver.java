package com.playground.designpattern.prototype;

public class Driver {
  public static void main(String[] args) {
    CloneFactory animalMaker = new CloneFactory();
    Sheep sheep = new Sheep();
    Sheep sheepClone = (Sheep) animalMaker.getClone(sheep);
    System.out.println(sheepClone);
    System.out.println(sheep);
    System.out.println(sheepClone.hashCode());
    System.out.println(sheep.hashCode());
    System.out.println("/********************");

    Lion lion = new Lion();
    Lion lionCopy = (Lion) animalMaker.getClone(lion);
    //or
    // lionCopy = lion.makeCopy();
    System.out.println(lion);
    System.out.println(lionCopy);
  }
}
