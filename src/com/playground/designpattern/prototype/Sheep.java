package com.playground.designpattern.prototype;

public class Sheep implements Animal{

  public Sheep() {
    System.out.println("Sheep is made!");
  }
  @Override
  public Animal makeCopy() {
    System.out.println("Sheeping is being made copy");
    Sheep sheep = null ;
    try {
      sheep = (Sheep) super.clone();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return sheep;
  }

  @Override
  public String toString() {
    return "sheep is my hero";
  }
}
