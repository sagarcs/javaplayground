package com.playground.designpattern.prototype;

public class CloneFactory {
  public Animal getClone(Animal animal) {
    return animal.makeCopy();
  }
}
