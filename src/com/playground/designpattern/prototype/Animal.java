package com.playground.designpattern.prototype;

public interface Animal extends Cloneable {
  public Animal makeCopy();
}
