package com.playground.designpattern.prototype;

public class Lion implements Animal {

  public Lion() {
    System.out.println("lion is being made!");
  }
  @Override
  public Animal makeCopy() {
    Lion lionClone = null;
    try {
      System.out.println("Lion copy is being made!");
      lionClone = (Lion) super.clone();
    } catch (Exception e ) {
      e.printStackTrace();
    }
    return lionClone;
  }
}
