package com.playground.designpattern.abstractfactory;

public class UFOBossEnemyShipFactory extends EnemyShipFactory {

  @Override
  public ESEngine addEngine() {
    return new UFOBossEngine();
  }

  @Override
  public ESWeapon addWeapon() {
    return new UFOBossWeapon();
  }
}
