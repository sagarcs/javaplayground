package com.playground.designpattern.abstractfactory;

public class UFOWeapon implements ESWeapon {
  @Override
  public String toString() {
    return "ufo weapon";
  }
}
