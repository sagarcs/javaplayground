package com.playground.designpattern.abstractfactory;

public class Driver {
  public static void main(String[] args) {
    EnemyShipBuilding ufoEnemyShipBuilding = new UFOEnemyShipBuilding();
    EnemyShip enemyShip = ufoEnemyShipBuilding.orderShip("boss");
    enemyShip.display();

    enemyShip = ufoEnemyShipBuilding.orderShip("ufo");
    enemyShip.display();

  }
}
