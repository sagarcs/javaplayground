package com.playground.designpattern.abstractfactory;

public class UFOBossEngine implements ESEngine {
  @Override
  public String toString() {
    return "BossEngine";
  }
}
