package com.playground.designpattern.abstractfactory;

public abstract class EnemyShipBuilding {

  public abstract EnemyShip makeShip(String type);

  public EnemyShip orderShip(String type) {
    EnemyShip enemyShip = makeShip(type);
    enemyShip.makeShip();
    return enemyShip;
  }
}
