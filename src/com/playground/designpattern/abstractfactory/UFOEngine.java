package com.playground.designpattern.abstractfactory;

public class UFOEngine implements ESEngine {
  @Override
  public String toString() {
    return "ufo engine";
  }
}
