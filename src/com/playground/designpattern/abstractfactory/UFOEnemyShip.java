package com.playground.designpattern.abstractfactory;

public class UFOEnemyShip extends EnemyShip {
  EnemyShipFactory enemyShipFactory;

  public UFOEnemyShip(EnemyShipFactory enemyShipFactory) {
    this.enemyShipFactory = enemyShipFactory;
  }

  @Override
  void makeShip() {
    System.out.println("making ufoenemyship: ");
    esEngine = enemyShipFactory.addEngine();
    esWeapon = enemyShipFactory.addWeapon();
  }
}
