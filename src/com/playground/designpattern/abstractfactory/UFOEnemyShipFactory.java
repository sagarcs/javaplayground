package com.playground.designpattern.abstractfactory;

public class UFOEnemyShipFactory extends EnemyShipFactory {

  @Override
  public ESEngine addEngine() {
    return new UFOEngine();
  }

  @Override
  public ESWeapon addWeapon() {
    return new UFOWeapon();
  }

}
