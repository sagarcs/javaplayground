package com.playground.designpattern.abstractfactory;

public class UFOBossEnemyShip extends EnemyShip {

  EnemyShipFactory enemyShipFactory;

  public UFOBossEnemyShip(EnemyShipFactory enemyShipFactory) {
    this.enemyShipFactory = enemyShipFactory;
  }

  @Override
  void makeShip() {
    System.out.println("making ufoboss ship:");
    esEngine = enemyShipFactory.addEngine();
    esWeapon = enemyShipFactory.addWeapon();
  }
}
