package com.playground.designpattern.abstractfactory;

public class UFOBossWeapon implements ESWeapon {
  @Override
  public String toString() {
    return "UFOBossWeapon";
  }
}
