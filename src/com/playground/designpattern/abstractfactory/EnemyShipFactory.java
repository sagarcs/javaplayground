package com.playground.designpattern.abstractfactory;

public abstract class EnemyShipFactory {
  public abstract ESEngine addEngine();
  public abstract ESWeapon addWeapon();
}
