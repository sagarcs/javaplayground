package com.playground.designpattern.abstractfactory;

public interface ESWeapon {
  public String toString();
}
