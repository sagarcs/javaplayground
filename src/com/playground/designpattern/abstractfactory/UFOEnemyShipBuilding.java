package com.playground.designpattern.abstractfactory;

public class UFOEnemyShipBuilding extends EnemyShipBuilding {

  @Override
  public EnemyShip makeShip(String type) {
    if(type.equals("ufo")) {
      EnemyShipFactory ufoEnemyShipFactory = new UFOEnemyShipFactory();
      EnemyShip enemyShip = new UFOEnemyShip(ufoEnemyShipFactory);
      return enemyShip;
    } else {
      EnemyShipFactory ufoBossEnemyShipFactory = new UFOBossEnemyShipFactory();
      EnemyShip enemyShip = new UFOBossEnemyShip(ufoBossEnemyShipFactory);
      return enemyShip;
    }
  }
}
