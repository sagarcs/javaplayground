package com.playground.designpattern.abstractfactory;

public abstract class EnemyShip {
  ESWeapon esWeapon;
  ESEngine esEngine;

  abstract void makeShip();

  public void display() {
    System.out.println(esWeapon.toString() + " weapon with :  " + esEngine + " engine");
  }

}
