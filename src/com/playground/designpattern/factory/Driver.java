package com.playground.designpattern.factory;

public class Driver {
  public static void main(String[] args) {
    ShipFactory shipFactory = new ShipFactory();
    EnemyShip enemyShip = shipFactory.makeShip("rocket");
    enemyShip.displayShip();
    enemyShip.followHeroShip();
    enemyShip.doDamage();

    System.out.println();

    enemyShip = shipFactory.makeShip("ufo");
    enemyShip.displayShip();
    enemyShip.followHeroShip();
    enemyShip.doDamage();


  }
}
