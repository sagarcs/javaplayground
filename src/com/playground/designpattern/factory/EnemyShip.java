package com.playground.designpattern.factory;

public abstract class EnemyShip {

  private String name;
  private double speed;
  private double amtDamage;

  public void followHeroship() {
    System.out.println(getName() + "is following heroship");
  }

  public void doDamage() {
    System.out.println(getName() + "is doing damage: " + amtDamage + " to the hero");
  }

  public void displayShip() {
    System.out.println(getName() + "is the ship");
  }


  public String getName() {
    return name;
  }

  public double getSpeed() {
    return speed;
  }

  public double getAmtDamage() {
    return amtDamage;
  }

  public void followHeroShip() {
    System.out.println(getName() + " is following the heroship:");
  }

  public void setName(final String name) {
    this.name = name;
  }

  public void setSpeed(final double speed) {
    this.speed = speed;
  }

  public void setAmtDamage(final double amtDamage) {
    this.amtDamage = amtDamage;
  }
}
