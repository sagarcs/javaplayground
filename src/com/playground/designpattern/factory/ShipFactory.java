package com.playground.designpattern.factory;

public class ShipFactory  {

  public EnemyShip makeShip(String type) {
    if (type.equals("rocket")) {
      return new RocketEnemyShip();
    } else if(type.equals("ufo")) {
      return new UFOEnemyShip();
    }
    return null;
  }

}
