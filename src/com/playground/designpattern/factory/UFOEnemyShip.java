package com.playground.designpattern.factory;

public class UFOEnemyShip extends EnemyShip {
  public UFOEnemyShip() {
    setName("ufo enemy ship");
    setSpeed(300);
    setAmtDamage(100);
  }
}
