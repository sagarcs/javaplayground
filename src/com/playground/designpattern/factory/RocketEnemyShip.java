package com.playground.designpattern.factory;

public class RocketEnemyShip extends EnemyShip {
  public RocketEnemyShip() {
    setAmtDamage(400);
    setSpeed(300);
    setName("Rocket enemy ship");
  }
}
