package com.playground.designpattern.composite;

public class Driver {
  public static void main(String[] args) {
    SongComponent song1 = new Song("SetFireTotheRain", "individual-adele", 2010);
    SongComponent song2 = new Song("It doesn't even matter", "individual-LinkinPark", 2000);
    SongComponent song3 = new Song("I guess I should go to sleep", "apple ad", 2019);

    SongComponent parentSongGroup = new SongGroup("industrialMusic","industrial music descr");
    SongComponent subSongGroup = new SongGroup("PopMusic","Pop music descr");


    subSongGroup.add(song1);
    subSongGroup.add(song2);
    parentSongGroup.add(subSongGroup);
    parentSongGroup.add(song3);

    DiscJockey discJockey = new DiscJockey(parentSongGroup);
    discJockey.playAllSongs();



  }
}
