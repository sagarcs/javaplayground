package com.playground.designpattern.composite;

import java.util.ArrayList;
import java.util.Iterator;

public class SongGroup extends SongComponent {

  ArrayList songComponents = new ArrayList();
  String groupName;
  String groupDescription;

  public SongGroup(String newGroupName, String newGroupDescr) {
    this.groupName = newGroupName;
    this.groupDescription = newGroupDescr;
  }

  public String getGroupName() {
    return groupName;
  }

  public String getGroupDescription() {
    return groupDescription;
  }

  public void add(SongComponent songComponent) {
    songComponents.add(songComponent);
  }

  @Override
  public void remove(final SongComponent newSongComponent) {
    songComponents.remove(newSongComponent);
  }

  @Override
  public SongComponent getComponent(final int index) {
    return (SongComponent) songComponents.get(index);
  }

  @Override
  public String getBandName() {
    return groupName;
  }

  @Override
  public void displaySongInfo() {
    System.out.println("songGroup: " + groupName);
    Iterator iterator = songComponents.iterator();
    System.out.println("*************");
    while(iterator.hasNext()) {
      SongComponent songComponent = (SongComponent) iterator.next();
      songComponent.displaySongInfo();
    }
    System.out.println("*************");
  }
}
