package com.playground.designpattern.composite;

import com.playground.designpattern.iterator.SongIterator;
import com.sun.xml.internal.ws.addressing.WsaActionUtil;
import java.util.Iterator;

public class Song extends SongComponent implements SongIterator {
  String songName;
  String bandName;

  @Override
  public Iterator getIterator() {
    return null;
  }

  int releaseYear;

  public Song(String songName, String bandName, int releaseYear) {
    this.songName = songName;
    this.bandName = bandName;
    this.releaseYear = releaseYear;
  }

  @Override
  public void displaySongInfo() {
    System.out.println("***");
    System.out.println("Song: " + songName);
    System.out.println("from: " + bandName);
    System.out.println("release : " + releaseYear);
    System.out.println("***");
  }
}
