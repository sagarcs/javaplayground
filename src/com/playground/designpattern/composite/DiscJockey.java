package com.playground.designpattern.composite;

public class DiscJockey {
  SongComponent songComponent;

  public DiscJockey(SongComponent songComponent) {
    this.songComponent = songComponent;
  }

  public void playAllSongs() {
    songComponent.displaySongInfo();
  }

}
