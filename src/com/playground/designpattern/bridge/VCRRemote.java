package com.playground.designpattern.bridge;

public class VCRRemote extends Remote {

  public VCRRemote(final EntertainmentDevice entertainmentDevice) {
    super(entertainmentDevice);
  }

  @Override
  void buttonNinePressed() {
    System.out.println("Pausing the video!");
  }

}
