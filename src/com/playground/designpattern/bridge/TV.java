package com.playground.designpattern.bridge;

public class TV extends EntertainmentDevice {

  public TV(int deviceState, int maxSetting) {
    this.deviceState = deviceState;
    this.maxSetting = maxSetting;
  }

  @Override
  public void buttonFivePressed() {
    deviceState--;
    System.out.println("moving backward in channel list!");
    feedBack();
  }

  @Override
  public void buttonSixPressed() {
    deviceState++;
    System.out.println("moving forward in channel List");
    feedBack();

  }
}
