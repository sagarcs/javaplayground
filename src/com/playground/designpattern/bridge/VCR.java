package com.playground.designpattern.bridge;

public class VCR extends EntertainmentDevice {

  public VCR(int videoInstant, int videoLength) {
    this.deviceState = videoInstant;
    this.maxSetting = videoLength;
  }

  @Override
  public void buttonFivePressed() {
    deviceState -= 10;
    System.out.println("back forward the video by 10 seconds");
    feedBack();
  }

  @Override
  public void buttonSixPressed() {
    deviceState += 10;
    System.out.println("forward the video! by 10 sec");
    feedBack();
  }


  public void feedBack() {
    if(deviceState > maxSetting || deviceState < 0) {
      deviceState = 0;
    }
    System.out.println("Video at: " + deviceState);
  }
}
