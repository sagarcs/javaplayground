package com.playground.designpattern.bridge;

public abstract class Remote {
  private EntertainmentDevice entertainmentDevice;
  public Remote(EntertainmentDevice entertainmentDevice) {
    this.entertainmentDevice  = entertainmentDevice;
  }

  public void buttonFivePressed() {
    entertainmentDevice.buttonFivePressed();
  }

  public void buttonSixPressed() {
    entertainmentDevice.buttonSixPressed();
  }

  abstract void buttonNinePressed() ;

}
