package com.playground.designpattern.bridge;

public class TVRemote extends Remote {

  public TVRemote(final EntertainmentDevice entertainmentDevice) {
    super(entertainmentDevice);
  }

  @Override
  void buttonNinePressed() {
    System.out.println("Muting the TV!");
  }

}
