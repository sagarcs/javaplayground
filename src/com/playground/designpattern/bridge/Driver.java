package com.playground.designpattern.bridge;

public class Driver {
  public static void main(String[] args) {
    EntertainmentDevice tv  = new TV(0, 100);
    EntertainmentDevice vcr = new VCR(0, 1000);
    Remote tvRemote = new TVRemote(tv);
    Remote vcrRemote = new VCRRemote(vcr);

    tvRemote.buttonFivePressed();
    tvRemote.buttonSixPressed();
    tvRemote.buttonNinePressed();

    System.out.println();

    vcrRemote.buttonSixPressed();
    vcrRemote.buttonFivePressed();
    vcrRemote.buttonNinePressed();

  }
}
