package com.playground.designpattern.bridge;

public abstract class EntertainmentDevice {
  public int deviceState;
  public int maxSetting;
  public int VolumeSetting = 0 ;
  public abstract void buttonFivePressed();
  public abstract void buttonSixPressed();

  public void feedBack() {
    if(deviceState > maxSetting || deviceState < 0) {
      deviceState = 0;
    }
    System.out.println("device is on : " + deviceState);
  }
  public void buttonSevenPressed() {
    VolumeSetting++;
    System.out.println("volume at : " + VolumeSetting);
  }

  public void buttonEightPressed() {
    VolumeSetting--;
    System.out.println("volume at : " + VolumeSetting);
  }

}