package com.playground.designpattern.chainofresponsibility;

public class NumberOperationRequest {
  public int number1, number2;
  public String request;

  public NumberOperationRequest(int number1, int number2, String request) {
    this.number1 = number1;
    this.number2 = number2;
    this.request = request;
  }
}
