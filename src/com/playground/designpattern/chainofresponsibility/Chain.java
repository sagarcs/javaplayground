package com.playground.designpattern.chainofresponsibility;

public interface Chain {
  public void setNextInChain(Chain nextInChain);
  public void handleRequest(NumberOperationRequest numberOperationRequest);
}
