package com.playground.designpattern.chainofresponsibility;

public class Substract implements Chain {
  Chain nextInChain;
  @Override
  public void setNextInChain(final Chain nextInChain) {
    this.nextInChain = nextInChain;
  }

  @Override
  public void handleRequest(NumberOperationRequest numberOperationRequest) {
    if(numberOperationRequest.request == "sub") {
      System.out.println(numberOperationRequest.number1 - numberOperationRequest.number2 + "is "
          + "the sub");
      System.out.println();
    } else {
      System.out.println("can't handle other than add or sub");
    }
  }
}
