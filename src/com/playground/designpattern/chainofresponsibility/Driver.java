package com.playground.designpattern.chainofresponsibility;

public class Driver {
  public static void main(String[] args) {
    Chain defaultHandler;
    Chain addNumber = new AddNumber();
    Chain subNumber = new Substract();
    addNumber.setNextInChain(subNumber);
    subNumber.setNextInChain(null);
    defaultHandler = addNumber;
    NumberOperationRequest numberOperationRequest1 = new NumberOperationRequest(1,2, "add");
    NumberOperationRequest numberOperationRequest2= new NumberOperationRequest(1,2, "sub");
    NumberOperationRequest numberOperationReques3= new NumberOperationRequest(1, 2, "div");

    defaultHandler.handleRequest(numberOperationRequest1);
    System.out.println();

    defaultHandler.handleRequest(numberOperationRequest2);
    System.out.println();

    defaultHandler.handleRequest((numberOperationReques3));
  }
}
