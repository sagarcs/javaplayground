package com.playground.designpattern.chainofresponsibility;

public class AddNumber implements Chain {
  Chain nextInChain;
  @Override
  public void setNextInChain(final Chain nextInChain) {
      this.nextInChain = nextInChain;
  }

  @Override
  public void handleRequest(NumberOperationRequest numberOperationRequest) {
    if (numberOperationRequest.request == "add") {
      System.out.println(numberOperationRequest.number1 + numberOperationRequest.number2 + " is "
          + "the sum");
    } else {
      System.out.println("please wait, as we forward this request!!");
      nextInChain.handleRequest(numberOperationRequest);
    }
  }
}
