package com.playground.designpattern.command;

public class TV implements ElectronicDevice {

  @Override
  public void turnOn() {
    System.out.println("TV is turned on");
  }

  @Override
  public void turnOff() {
    System.out.println("TV is turned off");
  }

  @Override
  public void sleep() {
    System.out.println("TV is in sleep mode");
  }

  @Override
  public void wakeup() {
    System.out.println("TV is waking up");
  }
}
