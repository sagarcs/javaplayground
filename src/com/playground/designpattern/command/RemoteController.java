package com.playground.designpattern.command;

import java.util.ArrayList;
import java.util.List;

public class RemoteController {
  List<Button> remoteButtons;

  public RemoteController() {
    remoteButtons = new ArrayList<>();
  }

  public void addButton(Button b) {
    remoteButtons.add(b);
  }

  public void pressButton(int number) {
    try {
      Button button = remoteButtons.get(number);
      button.pressButton();
    } catch (Exception e) {
      System.out.println("button with the number doesn't exist");
    }
  }

  public void unPressButton(int number) {
    try {
      Button button = remoteButtons.get(number);
      button.undoPressButton();
    } catch (Exception e) {
      System.out.println("button with the number doesn't exist");
    }
  }

}
