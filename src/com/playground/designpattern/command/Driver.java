package com.playground.designpattern.command;

public class Driver {

  public static void main(String[] args) {
    ElectronicDevice tv = new TV();
    ElectronicDevice radio = new Radio();

    TurnDeviceOnCommand turnTVDeviceOnCommand = new TurnDeviceOnCommand(tv);
    TurnDeviceOffCommand turnTVDeviceOffCommand = new TurnDeviceOffCommand(tv);

    TurnDeviceOnCommand turnRadioDeviceOnCommand = new TurnDeviceOnCommand(radio);
    TurnDeviceOffCommand turnRadioDeviceOffCommand = new TurnDeviceOffCommand(radio);

    Button tvButton1 = new Button(turnTVDeviceOffCommand);
    Button tvButton2 = new Button(turnTVDeviceOnCommand);

    Button radioButton1 = new Button(turnRadioDeviceOffCommand);
    Button radioButton2 = new Button(turnRadioDeviceOnCommand);

    RemoteController tvRemoteController = new RemoteController() ;
    tvRemoteController.addButton(tvButton1);
    tvRemoteController.addButton(tvButton2);

    RemoteController radioRemoteController = new RemoteController();
    radioRemoteController.addButton(radioButton1);
    radioRemoteController.addButton(radioButton2);

    tvRemoteController.pressButton(0);
    tvRemoteController.pressButton(1);

    radioRemoteController.pressButton(0);
    radioRemoteController.pressButton(1);
    radioRemoteController.pressButton(2);

    System.out.println();

    tvRemoteController.unPressButton(0);
    tvRemoteController.unPressButton(1);

    radioRemoteController.unPressButton(0);
    radioRemoteController.unPressButton(1);


  }






}
