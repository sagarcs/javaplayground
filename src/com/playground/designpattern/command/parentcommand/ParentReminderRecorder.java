package com.playground.designpattern.command.parentcommand;

import java.util.ArrayList;
import java.util.List;

public class ParentReminderRecorder {
    List<Command> remindersToKids;

    public ParentReminderRecorder() {
      this.remindersToKids = new ArrayList<>();
    }

    public void addReminder(Command command) {
      remindersToKids.add(command);
    }

    public void playReminders() {
      remindersToKids.stream().forEach(command -> command.execute());
    }

}
