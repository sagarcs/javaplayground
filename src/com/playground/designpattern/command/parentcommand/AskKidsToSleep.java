package com.playground.designpattern.command.parentcommand;

public class AskKidsToSleep implements Command {
  ParentsInstruction parentsInstruction;

  public AskKidsToSleep(ParentsInstruction parentsInstruction) {
    this.parentsInstruction = parentsInstruction;
  }

  @Override
  public void execute() {
    parentsInstruction.askKidsToSleep();
  }
}
