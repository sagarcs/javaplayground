package com.playground.designpattern.command.parentcommand;

public class MotherInstruction implements ParentsInstruction {
  @Override
  public void askKidsToEat() {
    System.out.println("mother asking kids to eat");
  }

  @Override
  public void askKidsToSleep() {
    System.out.println("mother asking kids to sleep!");
  }

}
