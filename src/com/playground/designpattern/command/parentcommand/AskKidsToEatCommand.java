package com.playground.designpattern.command.parentcommand;

public class AskKidsToEatCommand implements Command {

  ParentsInstruction parentsInstruction;

  public AskKidsToEatCommand(ParentsInstruction parentsInstruction) {
    this.parentsInstruction = parentsInstruction;
  }

  @Override
  public void execute() {
    parentsInstruction.askKidsToEat();
  }

}
