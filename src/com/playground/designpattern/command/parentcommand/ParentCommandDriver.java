package com.playground.designpattern.command.parentcommand;

public class ParentCommandDriver {
  public static void main(String[] args) {
    MotherInstruction motherInstruction1 = new MotherInstruction();
    FatherInstruction fatherInstruction1 = new FatherInstruction();

    AskKidsToEatCommand askKidsToEatCommand = new AskKidsToEatCommand(fatherInstruction1);
    AskKidsToSleep askKidsToSleep = new AskKidsToSleep(motherInstruction1);

    ParentReminderRecorder parentReminderRecorder = new ParentReminderRecorder();
    parentReminderRecorder.addReminder(askKidsToEatCommand);
    parentReminderRecorder.addReminder(askKidsToSleep);
    parentReminderRecorder.playReminders();

  }
}
