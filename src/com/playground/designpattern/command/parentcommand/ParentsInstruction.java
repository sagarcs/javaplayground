package com.playground.designpattern.command.parentcommand;

public interface ParentsInstruction {
  public void askKidsToEat();
  public void askKidsToSleep();
}
