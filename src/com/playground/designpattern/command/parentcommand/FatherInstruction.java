package com.playground.designpattern.command.parentcommand;

public class FatherInstruction implements ParentsInstruction {

  @Override
  public void askKidsToEat() {
    System.out.println("father asking kids to eat");
  }

  @Override
  public void askKidsToSleep() {
    System.out.println("father asking kids to sleep!");
  }

}
