package com.playground.designpattern.command.parentcommand;

public interface Command {
  public void execute();
}
