package com.playground.designpattern.command;

public interface ElectronicDevice {
  public void turnOn();
  public void turnOff();
  public void sleep();
  public void wakeup();
}
