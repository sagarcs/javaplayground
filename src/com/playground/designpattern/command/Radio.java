package com.playground.designpattern.command;

public class Radio implements ElectronicDevice{
  @Override
  public void turnOn() {
    System.out.println("radio is turned on");
  }

  @Override
  public void turnOff() {
    System.out.println("radio is turned off");
  }

  @Override
  public void sleep() {
    System.out.println("radio is sleep");
  }

  @Override
  public void wakeup() {
    System.out.println("radio is wakeup");
  }
}
