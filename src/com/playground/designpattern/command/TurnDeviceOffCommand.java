package com.playground.designpattern.command;

public class TurnDeviceOffCommand implements Command {
  ElectronicDevice device;

  public TurnDeviceOffCommand(ElectronicDevice device) {
    this.device = device;
  }

  @Override
  public void execute() {
    device.turnOff();
  }

  @Override
  public void unDo() {
    device.turnOn();
  }
}
