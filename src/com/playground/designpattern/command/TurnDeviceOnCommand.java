package com.playground.designpattern.command;

//Command
public class TurnDeviceOnCommand implements  Command {

  //receiver
  ElectronicDevice device;

  public TurnDeviceOnCommand(ElectronicDevice device) {
    this.device = device ;
  }

  @Override
  public void execute() {
    device.turnOn();
  }

  @Override
  public void unDo() {
    device.turnOff();
  }
}
