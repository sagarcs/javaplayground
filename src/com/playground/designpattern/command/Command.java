package com.playground.designpattern.command;

public interface Command {
  public void execute();
  public void unDo();
}
