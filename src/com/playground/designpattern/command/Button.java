package com.playground.designpattern.command;

//Button is invokerObject
public class Button {
  Command actionToPerform;

  public Button(Command actionToPerform) {
    this.actionToPerform = actionToPerform;
  }

  public void pressButton() {
    actionToPerform.execute();
  }

  public void undoPressButton() {
    actionToPerform.unDo();
  }

}
