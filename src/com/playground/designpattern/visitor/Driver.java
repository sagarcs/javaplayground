package com.playground.designpattern.visitor;

public class Driver {
  public static void main(String[] args) {
    Necessity necessity = new Necessity(100.0);
    Liquor liquor = new Liquor(200.0);
    Tobacco tobacco = new Tobacco(300.0);

    System.out.println("prices after tax:");
    System.out.println(tobacco.accept(new TaxVisitor()));
    System.out.println(liquor.accept(new TaxVisitor()));
    System.out.println(necessity.accept(new TaxVisitor()));

    System.out.println();

    System.out.println("prices after holiday tax");
    System.out.println(tobacco.accept(new TaxinHolidayVisitor()));
    System.out.println(liquor.accept(new TaxinHolidayVisitor()));
    System.out.println(necessity.accept(new TaxinHolidayVisitor()));

  }
}
