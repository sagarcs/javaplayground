package com.playground.designpattern.visitor;

public interface Visit {
  double accept(Visitor visitor);
}
