package com.playground.designpattern.visitor;

public class Tobacco implements Visit{
  public double price;
  public Tobacco(double price) {this.price = price; }
  public double getPrice() {
    return price;
  }

  @Override
  public double accept(final Visitor visitor) {
    return visitor.visit(this);
  }
}
