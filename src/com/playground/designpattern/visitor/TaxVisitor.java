package com.playground.designpattern.visitor;

public class TaxVisitor implements Visitor {
  @Override
  public double visit(final Liquor liquor) {
    return liquor.getPrice() * 1.5;
  }

  @Override
  public double visit(final Necessity necessity) {
    return necessity.getPrice() * 1.2;

  }

  @Override
  public double visit(final Tobacco tobacco) {
    return tobacco.getPrice() * 1.9;
  }
}
