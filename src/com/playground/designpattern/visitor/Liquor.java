package com.playground.designpattern.visitor;

public class Liquor implements Visit {

  public double price;
  public Liquor(double price) {
    this.price = price;
  }

  @Override
  public double accept(final Visitor visitor) {
    return visitor.visit(this);
  }

  public double getPrice() {
    return price;
  }
}
