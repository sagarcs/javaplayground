package com.playground.designpattern.visitor;

public class TaxinHolidayVisitor implements  Visitor {

  @Override
  public double visit(final Liquor liquor) {
    return liquor.getPrice() * 1.4;
  }

  @Override
  public double visit(final Necessity necessity) {
    return necessity.getPrice() * 1.2;

  }

  @Override
  public double visit(final Tobacco tobacco) {
    return tobacco.getPrice() * 1.6;
  }
}
