package com.playground.designpattern.visitor;

public class Necessity implements Visit{
  public double price ;
  public Necessity(double price) {
    this.price = price;
  }
  public double getPrice() {
    return price;
  }

  @Override
  public double accept(final Visitor visitor) {
    return visitor.visit(this);
  }
}
