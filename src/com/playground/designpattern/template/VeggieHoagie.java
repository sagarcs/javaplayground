package com.playground.designpattern.template;

import java.util.Arrays;

//original implementation extends just Hoagie, however extending ItalianHoagie does heavylifting
// for us
//public class VeggieHoagie extends Hoagie

public class VeggieHoagie extends ItalianHoagie {
  public String name = "Veggie Hoagie";
  String[] veggies = {"capsicum", "onion"};
  String[] cheeses = { "amul", "local"};

  public VeggieHoagie(final String name) {
    super(name);
  }

  @Override
  void addMeat() {
  }

  @Override
  boolean customerWantsMeat() {
    return false;
  }
}
