package com.playground.designpattern.template;

public abstract class Hoagie {

  public String name ;

  public Hoagie(String name) {
    this.name = name;
  }

  //mandatory behaviour - implementation remains same for subClasses
  //however
  final void makeSandwich() {
    System.out.println("making the " + name);
    cutBun();
    if(customerWantsMeat()) {
      addMeat();
    }
    if (customerWantsCheese()) {
      addCheese();
    }
    if(customerWantsVeggies()) {
      addVeggies();
    }
    wrapTheHoagie();
  }

  //-------------mandatory behaviour - implementation remains same for subclasses
  //i.e all hoagies need to be cut and wrapped
  protected final void cutBun() {
    System.out.println("cutting the bun!");
  }

  protected final void wrapTheHoagie() {
    System.out.println("wrapping the hoagie");
  }

  //------------ mandatory behaviour - implementation differs for subclasses
  abstract void addMeat();
  abstract void addVeggies();
  protected abstract void addCheese();

  // ---------------- default hoagie
  //these are called as hooks as well
  //override below to customise options!
  boolean customerWantsMeat() {
    return true;
  }

  boolean customerWantsCheese() {
    return true;
  }

  boolean customerWantsVeggies() {
    return true;
  }
}
