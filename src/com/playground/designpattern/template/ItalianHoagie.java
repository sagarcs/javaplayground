package com.playground.designpattern.template;

import java.util.Arrays;

public class ItalianHoagie extends Hoagie {

  public String name = "Italian Hoagie";
  String[] meats = {"meat1", "meat2"};
  String[] veggies = {"capsicum", "onion"};
  String[] cheeses = { "amul", "local"};

  public ItalianHoagie(final String name) {
    super(name);
  }

  @Override
  void addMeat() {
    System.out.println("adding meat:-");
    Arrays.stream(meats).forEach(meat -> System.out.println(meat));
  }

  @Override
  void addVeggies() {
    System.out.println("adding veggies:-");
    Arrays.stream(veggies).forEach(veg -> System.out.println(veg));
  }

  @Override
  protected void addCheese() {
    System.out.println("adding cheese:-");
    Arrays.stream(cheeses).forEach(cheese -> System.out.println(cheese));
  }
}
