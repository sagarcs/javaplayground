package com.playground.designpattern.template;

public class Driver {
  public static void main(String[] args) {

    ItalianHoagie italianHoagie = new ItalianHoagie("Italian Hoagie");
    italianHoagie.makeSandwich();
    System.out.println();
    VeggieHoagie veggieHoagie = new VeggieHoagie("veggie Haogie");
    veggieHoagie.makeSandwich();

  }
}
