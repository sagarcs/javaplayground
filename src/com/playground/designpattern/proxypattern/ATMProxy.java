package com.playground.designpattern.proxypattern;

import com.playground.designpattern.statemachine.ATMMachine;

public class ATMProxy implements ProtectedATMMachine {
  private ATMMachine atmMachine;

  public ATMProxy() {
    atmMachine = new ATMMachine();
  }
  public ATMProxy(ATMMachine atmMachine) {
    this.atmMachine = atmMachine;
  }

  @Override
  public String getATMState() {
    return atmMachine.getState();
  }

  @Override
  public int getATMBalance() {
    return atmMachine.getATMBalance();
  }
}
