package com.playground.designpattern.proxypattern;

import com.playground.designpattern.statemachine.ATMMachine;

public class Driver {
  public static void main(String[] args) {

    //impl1
    ProtectedATMMachine protectedATMMachine = new ATMMachine();
    System.out.println(protectedATMMachine.getATMBalance());
    System.out.println(protectedATMMachine.getATMState());
    System.out.println();

/*
    can't access other like withDrawCash:-
    1. protectedATMMachine.withdrawCash();
    or
    setCash
    2. protectedATMMachine.loadATMWithCash();

 */
   //impl2
    ATMProxy atmProxy = new ATMProxy();
    System.out.println(atmProxy.getATMBalance());
    System.out.println(atmProxy.getATMState());
    System.out.println();

    //impl3
    //the way you can send this over to other clients
    ATMProxy protectedATMProxy = getProxy();
    System.out.println(protectedATMProxy.getATMState());
    System.out.println(protectedATMProxy.getATMBalance());

  }

  private static ATMProxy getProxy() {
    ATMMachine hdfcATM  = new ATMMachine();
    hdfcATM.loadATMWithCash(1000);
    ATMProxy hdfcATMProxy = new ATMProxy(hdfcATM);
    return hdfcATMProxy;
  }

}
