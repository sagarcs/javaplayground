package com.playground.designpattern.proxypattern;

import com.playground.designpattern.statemachine.ATMState;


//this creates subclasses
//one subclass will be the proxyclass. i.e ATMProxy in Driver
//the other class will be the class that will be protected by the proxyClass. i.e ATMMachine in
// Driver

public interface ProtectedATMMachine {
  public String getATMState();
  public int getATMBalance();
}
