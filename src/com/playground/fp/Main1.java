package com.playground.fp;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

public class Main1 {
  public static void main(String[] args) {
    List<String> names = Arrays.asList("sagar", "sachin", "sharath", "nisagara", "nithing");

    for(String name: names) {
      if(!name.equals("sharath")) {
        System.out.println(name);
      }
    }
    System.out.println();

    names
        .stream()
        .filter(name -> !name.equals("sharath"))
        .forEach(name -> System.out.println(name));

    System.out.println();

    names
        .stream()
        .filter(Main1::notEqualToSharath)
        .forEach(System.out::println);

    names
        .stream()
        .filter(Main1::notEqualToSharath)
        .map(new Function<String, Object>() {

          @Override
          public Object apply(final String s) {
            return s.concat(s);
          }
        })
        .forEach(System.out::println);

    System.out.println();

    names
        .stream()
        .filter(Main1::notEqualToSharath)
        .map((Function<String, Object>) s -> s.concat(s))
        .forEach(System.out::println);

   List<User> userList =  names
        .stream()
        .filter(Main1::notEqualToSharath)
        .map(User::new)
        //for each is terminal
        .collect(Collectors.toList());

   int sum = userList.stream()
       .mapToInt(new ToIntFunction<User>() {
         @Override
         public int applyAsInt(final User value) {
           return value.getAge();
         }
       })
       .sum();

   System.out.println(sum + " is the sum");

   sum = userList.stream()
        .mapToInt(user -> 100)
        .sum();

    System.out.println(sum + " is the sum");


  }

  private static boolean notEqualToSharath(String s) {
    return  !s.equals("sharath");
  }

  private static class User {
    private String name;
    private int age = 40;

    public User(String name) {
      this.name = name.concat(name);
    }

    private int getAge() {
      return age;
    }

    @Override
    public String toString() {
      return name;
    }
  }
}
