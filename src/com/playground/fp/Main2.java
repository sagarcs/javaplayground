package com.playground.fp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Main2 {
  public static void main(String[] args) {

    List<User> users = Arrays.asList(
        new User("sagar", Arrays.asList(973812061, 12345)),
        new User("sachin", Arrays.asList(873812061, 12345)),
        new User("sagar", Arrays.asList(773812061, 12345)),
        new User("x", Arrays.asList(1,2))
        );
       users
        .stream()
        .map(user-> user.getPhoneNo())
           .filter(new Predicate<List<Integer>>() {
             @Override
             public boolean test(final List<Integer> integers) {
               return integers.contains(12345);
             }
           })
           .flatMap(Collection::stream)
           .forEach(System.out::println);
  }


  private static class User {
    private String name;
    private int age = 40;
    List<Integer> phoneNo;


    public User(String name, List<Integer> phoneNo) {
      this.phoneNo = phoneNo;
      this.name = name.concat(name);
    }

    private int getAge() {
      return age;
    }

    public List<Integer> getPhoneNo() {
      return phoneNo;
    }

    @Override
    public String toString() {
      return name;
    }
  }
}
