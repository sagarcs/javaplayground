package com.playground.tdd;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.Test;

public class PrimeFactorTest {
    @Test
    public void primeFactor() {
        assert (factorsOf(1).size() == 0);
        assertEquals(factorsOf(2), Arrays.asList(2));
        assertEquals(factorsOf(3), Arrays.asList(3));
        assertEquals(factorsOf(4), Arrays.asList(2, 2));
        assertEquals(factorsOf(5), Arrays.asList(5));

        assertEquals(factorsOf(6), Arrays.asList(2, 3));
        assertEquals(factorsOf(7), Arrays.asList(7));
        assertEquals(factorsOf(8), Arrays.asList(2, 2, 2));
        assertEquals(factorsOf(9), Arrays.asList(3, 3));
        assertEquals(factorsOf(2 * 3 * 5 * 7 * 2 * 2), Arrays.asList(2, 2, 2, 3, 5, 7 ));
    }

    private List<Integer> factorsOf(int number) {
        List<Integer> list = new ArrayList<>();
        if (number > 1) {
            int divisor = 2;
            while (number > 1) {
                while (number % divisor == 0) {
                    list.add(divisor);
                    number /= divisor;
                }
                divisor++;
            }
        }
        return list;
    }

}
