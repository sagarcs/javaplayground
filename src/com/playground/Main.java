package com.playground;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeoutException;

public class Main {

    public static void main2(String[] args) {
      String s1 = "\"";
      String s2 = "\u2033";
      System.out.println(s1.equals(s2));
      System.out.println("\u2033sagar\u20332033 ");

    }

  public static void main(String[] args) throws IOException {
    BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));
    String lineOfText = obj.readLine();
    String[] words = lineOfText.split(" ");
    Map<String, Integer> map = new HashMap<String, Integer>();
    int maximum = Integer.MIN_VALUE;
    String mostRepeating = "";
    for(String word: words) {
      if(map.containsKey(word)) {
        int count = map.get(word);
        count++;
        if(count > maximum) {
          maximum = count;
          mostRepeating = word;
        }
        map.put(word, count);
      } else {
        map.put(word, 1);
        if(1 > maximum) {
          maximum = 1;
          mostRepeating = word;
        }
      }
    }

    System.out.println(mostRepeating);

  }
}
